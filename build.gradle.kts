plugins {
    id("application")
}

group = "be.kdg.pro3.m01"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

application {
    mainClass.set("be.kdg.pro3.m01.Main")
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}
tasks.getByName("run",JavaExec::class){
    standardInput=System.`in`
}


tasks.test {
    useJUnitPlatform()
}