package be.kdg.pro3.m01;

import java.time.LocalDate;
import java.util.Random;

public class Actor {
	private static  Random random = new Random();
	private String name;
	private int birthYear;
	private Gender gender;

	public Actor(String name, int birthYear, Gender gender) {
		this.name = name;
		this.birthYear = birthYear;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}
	public int getAge() {
		return LocalDate.now().getYear() - getBirthYear();
	}

	public int getBirthYear() {
		return birthYear;
	}

	public Gender getGender() {
		return gender;
	}

	// example of a normal static factory method
	public static Actor createInstance(String name,Gender gender){
		return new Actor(name, LocalDate.now().getYear(), gender);
	}

	@Override
	public String toString() {
		return "Actor{" +
			"name='" + name + '\'' +
			", birthYear=" + birthYear +
			", gender=" + gender +
			'}';
	}

	public static Actor createRandomActor(){
		Gender gender = random.nextBoolean()?Gender.M:Gender.F;
		String name = ((gender == Gender.M)?"Keano":"Reece")
			+ String.format("%02d",random.nextInt(1,11));
		int birthYear=random.nextInt(1920,2011);
		return new Actor(name, birthYear, gender);
	}
}
