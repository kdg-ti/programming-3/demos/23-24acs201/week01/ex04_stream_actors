package be.kdg.pro3.m01;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) {
		// generate using lambda
		//List<Actor> actors =Stream.generate(() -> Actor.createRandomActor())
		// generate using method reference
		List<Actor> actors = Stream.generate(Actor::createRandomActor)
			.limit(20)
			.toList();
		System.out.println("All actors: " + actors);
		// filtering, converting to string, joining and printing
//		System.out.println("Female actors: "
//			+ actors.stream()
//			.filter(a -> a.getGender() == Gender.F)
//			.map(a -> a.toString())
//			.collect(Collectors.joining(", ")));
		// List of females
		List<Actor> females = actors.stream()
			.filter(a -> a.getGender() == Gender.F)
			.toList();
		// print the list
		System.out.println("Female actors: "
			+ females);

		// List of males above 50
		List<Actor> oldMales = actors.stream()
			.filter(a -> a.getGender() == Gender.M
				&& (a.getAge() > 50)
			)
			.toList();
		// print the list
		System.out.println("Old male actors: "
			+ oldMales);

		// List sorted by age
		List<Actor> sortedByAge = actors.stream()
			// appending reversed does not work without the cast
			.sorted(Comparator.comparing(a -> ((Actor)a).getBirthYear()).reversed())
			// same with method referenc
			// .sorted(Comparator.comparing(Actor::getBirthYear).reversed()
			.toList();
		// print the list
		System.out.println(">>> Actors sorted by age: "
			+ sortedByAge);


		System.out.println(">>> Total age of all female actors: "
			+ females.stream().mapToInt(a -> a.getAge()).sum());

		// List of males
		List<Actor> males = actors.stream()
			.filter(a -> a.getGender() == Gender.M)
			.toList();
		// print first letters of names
		System.out.println(">>> First letters of all males: "
			+ males.stream()
			.map(a -> a.getName().substring(0,1))
			.collect(Collectors.joining("")));


	}




}